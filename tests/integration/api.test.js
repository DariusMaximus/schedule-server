const request = require("supertest");
const { Schedule } = require("../../src/Models/timeslot.js");

let server;

describe("/api/appointments", () => {
  beforeEach(() => {
    server = require("../../src/index.js");
  });
  afterEach(async () => {
    server.close();
    await Schedule.remove({});
  });

  describe("GET /", () => {
    it("should return all appointments", async () => {
      await Schedule.collection.insertMany([
        { name: "Dar", date: "Sunday 08:00" },
        { name: "Gal", date: "Monday 10:00" },
      ]);

      const res = await request(server).get("/api/timeslots");

      expect(res.status).toBe(200);
      expect(res.body.length).toBe(2);
      expect(res.body.some((s) => s.name === "Dar")).toBeTruthy();
      expect(res.body.some((s) => s.name === "Gal")).toBeTruthy();
    });
  });

  describe("POST /", () => {
    it("should return posted appointment", async () => {
      const appointment = { date: "Sunday 08:00", name: "Dar" };

      const res = await request(server)
        .post("/api/timeslots")
        .send(appointment);

      expect(res.status).toBe(200);
      expect(res.body.name).toEqual("Dar");
      expect(res.body.date).toEqual("Sunday 08:00");
    });

    it("should return status 404 due to invalid path", async () => {
      const appointment = { date: "Sunday 08:00", name: "Dar" };

      const res = await request(server)
        .post("/api/timeslots/1")
        .send(appointment);

      expect(res.status).toBe(404);
    });

    it("should return status 400 due to invalid property", async () => {
      const appointment = { date: "Sunday 08:00", name: "D" };

      const res = await request(server)
        .post("/api/timeslots/")
        .send(appointment);

      expect(res.status).toBe(400);
    });
  });

  describe("PUT /:id", () => {
    const appointmentToSave = { date: "Sunday 08:00", name: "Dar" };

    it("should return positive response for valid update", async () => {
      const appointmentToUpdate = { date: "Sunday 08:00", name: "Darale" };

      await request(server).post("/api/timeslots/").send(appointmentToSave);

      const res = await request(server)
        .put(`/api/timeslots/${appointmentToUpdate.date}`)
        .send(appointmentToUpdate);

      expect(res.status).toBe(200);
      expect(res.body.ok).toEqual(1);
    });

    it("should return 404 status for no such appointment in DB", async () => {
      const appointmentToUpdate = { date: "Sunday 15:00", name: "Darale" };

      const res = await request(server)
        .put(`/api/timeslots/`)
        .send(appointmentToUpdate);

      expect(res.status).toBe(404);
    });

    it("should return 400 status for property charactered short", async () => {
      const appointmentToUpdate = { date: "Sunday 08:00", name: "D" };

      await request(server).post("/api/timeslots/").send(appointmentToSave);

      const res = await request(server)
        .put(`/api/timeslots/${appointmentToUpdate.date}`)
        .send(appointmentToUpdate);

      expect(res.status).toBe(400);
    });
  });

  describe("DELETE /:id", () => {
    it("should return positive response for valid delete", async () => {
      const appointmentToSave = { date: "Sunday 08:00", name: "Dar" };

      await request(server).post("/api/timeslots/").send(appointmentToSave);

      const res = await request(server).delete(
        `/api/timeslots/${appointmentToSave.date}`
      );

      expect(res.body.ok).toBe(1);
    });
  });
});
