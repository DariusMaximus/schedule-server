const DB_PATH_PROD = "mongodb://localhost/hair-salon-db";
const DB_PATH_TEST = "mongodb://localhost/hair-salon-db-test";
const SERVER_PORT = 5000;
const GET_ALL_FROM_DB_PATH = "/api/timeslots";
const LOG_FILE_PATH = "defaultLogFile.log";
const UNHANDLED_EX_LOG_PATH = "unhandledExceptions.log";
const IS_DEV_ENV = false;

module.exports = {
  DB_PATH_PROD,
  DB_PATH_TEST,
  SERVER_PORT,
  GET_ALL_FROM_DB_PATH,
  LOG_FILE_PATH,
  UNHANDLED_EX_LOG_PATH,
  IS_DEV_ENV,
};
