module.exports = function asyncMiddleware(handler) {
  return async (res, req, next) => {
    try {
      await handler();
    } catch (ex) {
      next(ex);
    }
  };
};
