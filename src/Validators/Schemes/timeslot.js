const Joi = require("joi");

const schema = {
  name: Joi.string().min(3).max(20).required(),
  date: Joi.string().required(),
};

module.exports = { schema };
