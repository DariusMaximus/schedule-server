const Joi = require("joi");

function validateData(data, schema) {
  return Joi.validate(data, schema);
}

exports.validate = validateData;
