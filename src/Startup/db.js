const mongoose = require("mongoose");
const winston = require("winston");
const config = require("../Config/config");

module.exports = function () {
  const db = config.IS_DEV_ENV ? config.DB_PATH_TEST : config.DB_PATH_PROD;
  mongoose.connect(db).then(() => winston.log(`Connected to ${db}...`));
};
