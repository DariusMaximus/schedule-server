require("express-async-errors");
const winston = require("winston");
const config = require("../Config/config.js");

module.exports = function () {
  winston.handleExceptions(
    new winston.transports.File({ filename: config.UNHANDLED_EX_LOG_PATH })
  );

  process.on("unhandledRejection", (ex) => {
    throw ex;
  });

  winston.add(winston.transports.File, { filename: config.LOG_FILE_PATH });
};
