const cors = require("cors");
const express = require("express");
const customers = require("../Routes/api");
const error = require("../Middlewares/error");
const config = require("../Config/config");

module.exports = function (app) {
  app.use(express.json());
  app.use(cors());
  app.use(config.GET_ALL_FROM_DB_PATH, customers);
  app.use(error);
};
