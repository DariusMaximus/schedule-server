const express = require("express");
const winston = require("winston");
const config = require("./Config/config");
const app = express();

require("./Startup/logging.js")();
require("./Startup/middlewares.js")(app);
require("./Startup/db.js")();

const port = process.env.PORT || config.SERVER_PORT;
const server = app.listen(port, () =>
  winston.log(`Listening on port ${port}...`)
);

module.exports = server;
