const express = require("express");
const { Schedule } = require("../Models/timeslot.js");
const { validate } = require("../Validators/timeslot.js");
const { schema } = require("../Validators/Schemes/timeslot.js");
const router = express.Router();

router.get("/", async (req, res) => {
  const timeslots = await Schedule.find().sort("name");
  res.send(timeslots);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body, schema);
  if (error) {
    return res.status(400).send(error.details[0].message);
  }

  let timeslot = new Schedule({
    name: req.body.name,
    date: req.body.date,
  });
  timeslot = await timeslot.save();

  res.send(timeslot);
});

router.put("/:id", async (req, res) => {
  const { error } = validate(req.body, schema);
  if (error) return res.status(400).send(error.details[0].message);

  const timeslot = await Schedule.update(
    { date: req.params.id },
    {
      $set: {
        name: req.body.name,
      },
    },
    { new: true }
  );

  if (!timeslot)
    return res
      .status(404)
      .send("The timeslot with the given ID was not found.");

  res.send(timeslot);
});

router.delete("/:id", async (req, res) => {
  const timeslot = await Schedule.deleteMany({
    date: req.params.id,
  });

  if (!timeslot)
    return res
      .status(404)
      .send("The timeslot with the given ID was not found.");

  res.send(timeslot);
});

module.exports = router;
